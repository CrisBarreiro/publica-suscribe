package client;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import GUI.ClientGUI;

@SuppressWarnings("serial")
public class ClientBehaviour extends CyclicBehaviour {

	private ClientGUI clientGUI;

	public ClientBehaviour(ClientGUI clientGUI) {
		this.clientGUI = clientGUI;
	}

	public ClientBehaviour(Agent a) {
		super(a);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void action() {
		MessageTemplate mt = MessageTemplate.and(
				MessageTemplate.MatchPerformative(ACLMessage.INFORM),
				MessageTemplate.MatchConversationId("number-generation"));

		ACLMessage message = myAgent.receive(mt);
		if (message != null) {
			System.out.println(myAgent.getLocalName() + " "
					+ message.getContent());
			clientGUI.appendNumber("\t" + message.getContent() + "\n");
		}
	}

}
