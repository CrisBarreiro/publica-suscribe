package client;

import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import GUI.ClientGUI;
import GUI.Subscribe;

@SuppressWarnings("serial")
public class Client extends Agent {

	AID server;

	protected void setup() {
		Subscribe subscribe = new Subscribe(this);
		subscribe.setVisible(true);
		DFAgentDescription template = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setType("number-generation");
		template.addServices(sd);
		boolean flag = false;
		try {
			while (flag == false) {
				DFAgentDescription[] result = null;
				result = DFService.search(this, template);
				System.out.println(getAID().getLocalName()
						+ " BUSQUEDA REALIZADA");
				for (DFAgentDescription ad : result) {
					server = ad.getName();
					if (ad.getName() != null) {
						flag = true;
					}
				}
			}

		} catch (FIPAException e) {
			System.out.println(e.getMessage());
		}
	}

	public void subscribe(String time) {
		ACLMessage message = new ACLMessage(ACLMessage.SUBSCRIBE);
		message.setSender(this.getAID());
		message.addReceiver(server);
		message.setConversationId("number-generation");
		message.setContent(time);
		send(message);

		ClientGUI clientGUI = new ClientGUI();
		clientGUI.setVisible(true);
		addBehaviour(new ClientBehaviour(clientGUI));
	}

	protected void takedown() {

	}

}
