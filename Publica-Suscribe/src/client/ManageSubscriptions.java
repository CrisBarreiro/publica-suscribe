package client;

import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import server.Server;

@SuppressWarnings("serial")
public class ManageSubscriptions extends CyclicBehaviour {

	private Server server;

	public ManageSubscriptions() {
		// TODO Auto-generated constructor stub
	}

	public ManageSubscriptions(Server a) {
		super(a);
		this.server = a;
	}

	@Override
	public void action() {
		MessageTemplate mt = MessageTemplate.and(
				MessageTemplate.MatchPerformative(ACLMessage.SUBSCRIBE),
				MessageTemplate.MatchConversationId("number-generation"));
		ACLMessage message = myAgent.receive(mt);
		if (message != null) {
			System.out.println("Recibida petici�n de suscripci�n de: "
					+ message.getSender().getLocalName());
			AID subscriber = message.getSender();
			Boolean flag = server.addSubscriber(message.getSender(),
					Integer.parseInt(message.getContent()));
			if (flag == true) {
				message = new ACLMessage(ACLMessage.AGREE);
			} else {
				message = new ACLMessage(ACLMessage.REFUSE);
			}
			message.addReceiver(subscriber);
			message.setConversationId("number-generation");
			myAgent.send(message);
		} else {
			block();
		}
	}

}
