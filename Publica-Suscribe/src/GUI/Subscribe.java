package GUI;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import client.Client;

@SuppressWarnings("serial")
public class Subscribe extends JFrame {

	private JPanel contentPane;
	private JTextField time;
	@SuppressWarnings("unused")
	private Client client;
	private JButton btnSuscribirse;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Subscribe frame = new Subscribe();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Subscribe() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 282, 113);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Tiempo (s)");
		lblNewLabel.setBounds(26, 27, 61, 14);
		contentPane.add(lblNewLabel);

		time = new JTextField();
		time.setBounds(86, 24, 44, 20);
		contentPane.add(time);
		time.setColumns(10);

		btnSuscribirse = new JButton("Suscribirse");
		btnSuscribirse.setBounds(140, 23, 89, 23);
		contentPane.add(btnSuscribirse);
	}

	public Subscribe(Client client) {
		this();
		this.client = client;
		btnSuscribirse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				client.subscribe(time.getText());
				dispose();
			}
		});
	}
}
