package server;

import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

import java.util.HashMap;

import client.ManageSubscriptions;

@SuppressWarnings("serial")
public class Server extends Agent {

	private HashMap<AID, Integer> subscribers;

	protected void setup() {

		subscribers = new HashMap<>();
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(this.getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("number-generation");
		sd.setName("JADE-number-generation");
		dfd.addServices(sd);
		try {
			DFService.register(this, dfd);
		} catch (FIPAException e) {
			System.out.println(e.getMessage());
		}

		addBehaviour(new ManageSubscriptions(this));
		addBehaviour(new ServerBehaviour(this, 500));
	}

	protected void takedown() {
		System.out.println(getAID().getLocalName() + " finalizado");
	}

	public HashMap<AID, Integer> getSubscribers() {
		return subscribers;
	}

	public void setSubscribers(HashMap<AID, Integer> subscribers) {
		this.subscribers = subscribers;
	}

	public boolean addSubscriber(AID subscriber, int time) {
		if (!subscribers.keySet().contains(subscriber)) {
			subscribers.put(subscriber, time * 2);
			System.out.println(subscriber.getLocalName() + " se ha suscrito");
			return true;
		} else {
			System.out
					.println(subscriber.getLocalName() + " no se ha suscrito");
			return false;
		}
	}

	public void substractMessage(AID subscriber) {
		subscribers.put(subscriber, subscribers.get(subscriber) - 1);
	}

}
