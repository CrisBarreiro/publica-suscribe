package server;

import jade.core.AID;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;

import java.util.HashMap;

@SuppressWarnings("serial")
public class ServerBehaviour extends TickerBehaviour {

	HashMap<AID, Integer> subscribers;
	Server server;

	public ServerBehaviour(Server a, long period) {
		super(a, period);
		subscribers = new HashMap<>();
		server = a;
	}

	@Override
	protected void onTick() {
		subscribers = server.getSubscribers();
		if (!subscribers.isEmpty()) {
			ACLMessage message = new ACLMessage(ACLMessage.INFORM);
			message.setConversationId("number-generation");
			message.setSender(server.getAID());
			message.setContent(String.valueOf(Math.random()));
			for (AID subscriber : subscribers.keySet()) {
				if (subscribers.get(subscriber) > 0) {
					message.addReceiver(subscriber);
				}
			}
			for (AID subscriber : subscribers.keySet()) {
				server.substractMessage(subscriber);
				if (subscribers.get(subscriber) == 0) {
					System.out.println("Se ha revocado la suscripción de "
							+ subscriber.getLocalName());
				}
			}
			server.send(message);
		}
	}
}
