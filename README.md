# README #

Modelo Publica-Suscribe implementado utilizando Java RMI en el marco de la asignatura de Computación Distribuida

### Enunciado del ejercicio ###

Diseñar una aplicación que se rija por el modelo publica suscribe para la transmisión de datos de un proceso en tiempo real. Para ello, el servidor, una vez arrancado, deberá generar números aleatorios entre 0 y 1 con una frecuencia de 2 Hz y permanecerá generando números aleatorios hasta que concluya la aplicación. Los clientes se suscribirán al servidor, indicando que están interesados en recibir datos durante un tiempo determinado, en segundos. Pasado ese tiempo, el cliente dejará de recibir datos. En cualquier momento el cliente puede solicitar la renovación de la suscripción, que comenzará a contar a partir del instante en el que se efectúa la petición al servidor. También puede cancelar la recepción de datos en cualquier momento.